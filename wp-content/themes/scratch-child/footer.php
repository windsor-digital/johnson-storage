<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
?>

		</div><!-- #main -->
		</div><!-- #page -->
		<footer id="colophon" class="site-footer" role="contentinfo">

			<?php get_sidebar( 'footer' ); ?>

			<div class="site-info">
                <div class="footer-wrapper">
	                <div class="footer-left">
		                <div class="footer-nav">
			                <a href="/privacy" class="util-footer">Privacy Policy &amp; Anti-Bribery</a>&nbsp; | &nbsp;
			                <a href="/environmental-care" class="util-footer">Environmental Care</a>&nbsp; | &nbsp;       
			                <span>
				                <a href="/about-us/contact-us/" class="util-footer">Contact Us</a>&nbsp; | &nbsp;
				                <a href="/claim-form" class="util-footer">Claim Form</a>&nbsp; | &nbsp;
				                <a href="/site-map" class="util-footer">Site Map</a>
			                </span>
							<div class="footer-copyright">&copy; <?php echo date('Y'); ?> Johnson Storage and Moving | United Van Lines, LLC. All rights reserved.

USDOT #077949</div>
						</div>
	                </div>
	                <div class="footer-right">
	                	<div class="footer-logo"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/logo-footer.png" /></div>
		                <ul class="footer-social">
			                <li><a href="https://www.facebook.com/pages/Johnson-Storage-and-Moving-Agent-for-United-Centennial-CO/910309195656720 "><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-facebook.png" class="footer-social-icon" /></a></li>
			                <li><a href="https://twitter.com/JohnsonStorage"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-twitter.png" class="footer-social-icon" /></a></li>
			                <li><a href="http://www.linkedin.com/company/johnson-storage-&-moving"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-linkedin.png" class="footer-social-icon" /></a></li>
			                <li><a href="https://www.youtube.com/user/JohnsonUnited26"><img src="<?php echo get_stylesheet_directory_uri() ?>/images/icon-youtube.png" class="footer-social-icon" /></a></li>
		                </ul>
		                
	                </div>
                </div>
			</div><!-- .site-info -->
		</footer><!-- #colophon -->
	
	<?php wp_footer(); ?>
</body>
</html>