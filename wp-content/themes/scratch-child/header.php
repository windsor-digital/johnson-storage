<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php
	$favicon = fw_get_db_settings_option('favicon');
	if( !empty( $favicon ) ) :
	?>
	<link rel="icon" type="image/png" href="<?php echo $favicon['url'] ?>">
	<?php endif ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<?php wp_head(); ?>

	

    <style>
     @media screen and (max-width: 768px) {
     html { margin-top: 0px !important; }
     }
    </style>
	<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google­analytics.com/analytics.js','ga');
	ga('create', 'UA­83743370­1', 'auto');
	ga('send', 'pageview');
	</script>
</head>

<?php if(is_page()) { $page_slug = 'page-'.$post->post_name; } ?>

<body <?php body_class($page_slug); ?>>
<?php if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } ?>
<div class="header-top-wrapper">
				<div class="primary-navigation mobile-nav-top"><button class="menu-toggle"><?php _e( 'Primary Menu', 'unyson' ); ?></button>
				<a class="screen-reader-text skip-link" href="#content"><?php _e( 'Skip to content', 'unyson' ); ?></a></div>
	<div id="site-header">
    <div class="logo-top-wrapper"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="/wp-content/themes/scratch-child/images/logo-johnson-storage-moving.png" alt="<?php echo esc_attr(get_bloginfo('name')); ?>" class="logo"></a></div>
    <div class="header-right"><div class="util-nav"><a href="/instant-quote/" class="util-link">Instant Quote</a> &nbsp; | &nbsp; <a href="/about-us" class="util-link">About Us</a> &nbsp; | &nbsp; <a href="/about-us/contact-us/" class="util-link">Contact Us</a></div>
    <div class="header-phone-lg">1.800.BUY.MOVE</div>
    <div class="header-phone-sm">1.800.289.6683</div>
    <br />
    </div><!-- .header-right -->
    <!--div style="clear:both;"></div-->
    </div>
    <div class="header-mobile-quote" style="clear:both;"><a href="/instant-quote/" class="header-mobile-quote-button">Get an instant quote</a></div>
 </div><!-- .header-top-wrapper -->
	<header id="masthead" class="site-header" role="banner">
		<div class="header-main">
		<nav id="primary-navigation" class="site-navigation primary-navigation" role="navigation">            
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>

	</header>
	<!-- #masthead -->
	<?php
	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	$thumbnail_id = get_post_thumbnail_id($post->ID);
	$thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment')); 
	
	if ( is_front_page() ) {
	?>
		<div class="header-banner"  style="background-image: url('<?php echo $image[0]; ?>')">
	  		<div class="home-featured-text">
	  			<div class="header-featured-title"><?php the_subtitle(); ?></div>
				<div class="header-featured-tagline"><?php the_subheading(); ?></div>
			</div><!-- .header-banner -->
		</div><!-- .home-featured-text -->
	<?php 
	} else {
		?>
		<div class="header-featured-text-internal blog-header-banner">
	  				<div class="header-featured-title-internal">
	  			<span>Our goal is to make your move</span>
				<span>go as smoothly as possible</span>
			</div><!-- .header-banner -->
		</div><!-- .home-featured-text -->		
	<?php 
	}
	 ?>
  	
    <div id="page" class="hfeed site">
	
    <div id="main" class="site-main">
