<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<div id="content" role="main" style="text-align: center;padding: 110px 0 20px 0;">

			<header class="page-header">
				<h1 class="entry-title">Page not found</h1>
			</header>

			<div class="page-content">
				<p class="fourohfour"><?php _e( 'The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'unyson' ); ?></p>
<p>Please return to the <a href="/">homepage</a> or view our entire <a href="/site-map">site map</a>.</p>
<div class="longpadding" style="padding-bottom:200px"></div>
			</div><!-- .page-content -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php
get_sidebar( 'content' );
get_sidebar();
get_footer();
