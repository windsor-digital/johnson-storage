<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'Thinstrip Image Promo', 'fw' ),
		'description' => __( 'Thinstrip image promo', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' ),
	)
);
