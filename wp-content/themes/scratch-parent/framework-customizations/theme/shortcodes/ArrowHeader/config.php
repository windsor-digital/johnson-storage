<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'layout_builder' => array(
		'title'         => __('ArrowHeader', 'fw'),
		'description'   => __('Make special arrow headings', 'fw'),
		'tab'           => __('Content Elements', 'fw'),
	)
);
