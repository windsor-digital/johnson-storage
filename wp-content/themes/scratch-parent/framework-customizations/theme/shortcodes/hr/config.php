<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'layout_builder' => array(
		'title'         => __('HR / Spacer', 'fw'),
		'description'   => __('Creates an hr or cleared row with spacing', 'fw'),
		'tab'           => __('Content Elements', 'fw'),
		'popup_size'     => 'small'
	)
);