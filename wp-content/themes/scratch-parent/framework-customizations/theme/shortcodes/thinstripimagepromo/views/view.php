<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

/**
 * @var array $atts
 */

if ( empty( $atts['image'] ) ) {
	return;
}
$image = $atts['image']['url'];

?>
<div class="thinstrip-text-block ">
	<div class="shortcode-image-container">
		<?php if ( empty( $atts['link'] ) ) : ?>
			<img src="<?php echo $image ?>" alt="<?php echo $image ?>" class="shortcode-container"/>
		<?php else : ?>
			<a href="<?php echo $atts['link'] ?>" class="shortcode-container">
				<img src="<?php echo $image ?>" alt="<?php echo $image ?>" />
			</a>
		<?php endif ?>
	</div>
	<div class="shortcode-text-container">
		<span class="title"><?php echo $atts['text'] ?></span>
	</div>
	<div class="clearfix"></div>
</div>
