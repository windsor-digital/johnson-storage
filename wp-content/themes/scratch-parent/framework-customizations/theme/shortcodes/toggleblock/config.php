<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'ToggleBlock', 'fw' ),
		'description' => __( 'Create an block that will toggle open/closed', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' ),
	)
);