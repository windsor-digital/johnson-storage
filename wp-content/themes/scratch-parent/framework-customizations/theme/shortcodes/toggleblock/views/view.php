<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}
?>
<h3 class="toggle-title"><?php echo $atts['title']; ?><span class="toggle toggle-title  ui-icon"></span></h3>
<div class="toggle toggler shortcode-container <?php echo $atts['button_featured'] ?>">
<p><?php echo $atts['text'] ?></p>
</div>