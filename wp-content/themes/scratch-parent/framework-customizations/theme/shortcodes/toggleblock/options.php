<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'title'      => array(
		'type'    => 'group',
		'options' => array(
			'title'    => array(
				'type'  => 'text',
				'label' => __( 'Toggle Block Title', 'fw' ),
				'desc'  => __( 'Write the heading title content', 'fw' ),
			),
		)
	),
	'text' => array(
		'type'   => 'wp-editor',
		'teeny'  => false,
		'reinit' => true,
		'label'  => __( 'Content', 'fw' ),
		'desc'   => __( 'Enter some content for this texblock', 'fw' )
		
	),
	'button_featured' => array(
        'type'  => 'switch',
		'label'   => __( 'Default state', 'fw' ),
		'desc'    => __( 'Select here if you would like this open by default', 'fw' ),
        'right-choice' => array(
            'value' => 'active',
            'label' => __('Open', 'fw'),
        ),
        'left-choice' => array(
            'value' => 'inactive',
            'label' => __('Closed', 'fw'),
        ),
    ),
);