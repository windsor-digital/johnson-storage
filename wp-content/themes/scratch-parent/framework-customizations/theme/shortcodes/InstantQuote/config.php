<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$cfg = array(
	'layout_builder' => array(
		'title'       => __( 'InstantQuote', 'fw' ),
		'description' => __( 'Instant Quote tool form', 'fw' ),
		'tab'         => __( 'Content Elements', 'fw' ),
	)
);
