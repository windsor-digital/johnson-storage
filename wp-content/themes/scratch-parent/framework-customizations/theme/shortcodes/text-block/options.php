<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'textblock_size' => array(
        'type'  => 'switch',
		'label'   => __( 'Textblock Size', 'fw' ),
		'desc'    => __( 'Do you want this text box half size?', 'fw' ),
        'right-choice' => array(
            'value' => 'halfsize',
            'label' => __('Yes', 'fw'),
        ),
        'left-choice' => array(
            'value' => '',
            'label' => __('No', 'fw'),
        ),
    ),
	'text' => array(
		'type'   => 'wp-editor',
		'teeny'  => false,
		'extended'  => true,
		'reinit' => true,
		'label'  => __( 'Content', 'fw' ),
		'desc'   => __( 'Enter some content for this texblock', 'fw' )
	)
);
