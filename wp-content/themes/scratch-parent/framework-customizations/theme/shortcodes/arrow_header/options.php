<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$options = array(
	'content'      => array(
		'type'    => 'group',
		'options' => array(
			'title'    => array(
				'type'  => 'text',
				'label' => __( 'Arrow Heading Title', 'fw' ),
				'desc'  => __( 'Write the heading title content', 'fw' ),
			),
			'headline_link'   => array(
				'label' => __( 'headline Link', 'fw' ),
				'desc'  => __( 'Where should your headline link to', 'fw' ),
				'type'  => 'text',
				'value' => '#' 
			),
			'subtitle' => array(
				'type'  => 'text',
				'label' => __( 'Arrow Heading Subtitle', 'fw' ),
				'desc'  => __( 'Write the heading subtitle content', 'fw' ),
			),
		)
	),
	'icon' => array(
		'type'    => 'select',
		'label'   => 'Choose Your icon',
		'choices' => array(
			'empty' => 'empty',
			'truck' => 'truck',
			'safe' => 'safe',
			'building' => 'building',
			'map' => 'map',
		)
	),

);	