<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
} ?>
<div class="instant-quote shortcode-container">
	<div class="quoteheader">
		<span class="topline">INSTANT</span>
		<span class="Secondline">ONLINE QUOTE</span>
	</div>
	<?php echo do_shortcode( $atts['text'] ); ?>
	<div id="quote_form" class="form">
		<div class="column-1-2">
			<label for "addr_from">From ZIP Code </label>
			<input type="text" name="addr_from" name="addr_from" id="addr_from">
		</div>
		<div class="column-1-2 right">
			<label for "addr_to">To ZIP Code</label>
			<input type="text" name="addr_to" id="addr_to">
		</div>
		<div class="column-1-2">
			<label for "num_rooms">Room Count <abbr rel="tooltip" title="How many rooms?  Include all the rooms and areas in your home, including the kitchen, basement, garage, bedrooms, etc. For Example:3 bedrooms, 2 bathrooms, family room, kitchen, living room, garage, basement, office = 11 rooms">?</abbr></label>
			<select name="num_rooms" id="num_rooms">
				<option value="">Choose One</option>
				<?php $i = 1; while ($i++ < 20) { ?>
				<option value="<?php echo $i?>"><?php echo $i?> <?php echo ($i > 1) ? 'Rooms' : 'Room'?></option>
				<?php } ?>
			</select>
			
		</div>
		<div class="column-1-2 right">
			<label for "source">How you found us</label>
			<select name="source" id="source">
				<option value="">Choose One</option>
				<option value="Realtor Office/Kiosk">Realtor Office/Kiosk</option>
				<option value="Realtor Referral">Realtor Referral</option>
				<option value="Internet Search">Internet Search</option>
				<option value="Friend/Family">Friend/Family</option>
				<option value="Radio Ad">Radio Ad</option>
				<option value="Other">Other</option>
			</select>
			
		</div>
		<div class="clearfix"></div>
		<label for "full_name">Name</label>
		<input type="text" name="full_name" id="full_name">
		<label for "email">Email</label>
		<input type="text" name="email" id="email">
		<label for "phone">Phone</label>
		<input type="text" name="phone" id="phone">
		<div style="display:none">
			<input name="getquote" type="submit" value="getQuote">
		</div>
		<div id="error_div">
</div>
		<button class="getQuoteBtn" onclick="get_quote_json(); return false; ">GET QUOTE</button>
	</div>
</div>


<div style="display:none;">
	<form id="result_set" action="/quote-results/" method="POST">
	</form>
</div>

<script type="text/javascript">
	function get_quote_json() {
		
		//var quote_query = $('#quote_form').serialize();
		var quote_data = {};
		jQuery('#error_div').empty();
		jQuery('.getQuoteBtn').addClass('loading');
		jQuery('#quote_form').find('input, select').each(function(){
			quote_data[jQuery(this).attr('name')] = jQuery(this).val();
		});
		
				jQuery.ajax({
            url: '/api/quote-app/app/jsonresult.php',
            data: quote_data, 
            type: "POST",
            success: function(data) {
            	quote_result = JSON.parse(data);
            	console.log(quote_result);
            	if (quote_result.success) {
            		submitResults(quote_result);
            	} else {
        			for(var i=0; i < quote_result.errors.length; i++) {
        				jQuery('.getQuoteBtn').removeClass('loading');
            			jQuery('#error_div').append('<span>' + quote_result.errors[i] + '<span><br/>');	
            		}
            	}
            }
        });
	}

	function submitResults(quote_results) {
		data_rows = "";
		for (var key in quote_results) {
			if(key == 'containsValue') {
				data_row = "";
			} else if(key == 'map') { 
				data_row = "<textarea name='" + key + "' >" + quote_results[key] + "</textarea>";
			} else {
				data_row = "<input name='" + key + "' type='text' value='" + quote_results[key] + "'>";
			}
			data_rows = data_rows + data_row;			
		}
		jQuery('#result_set').html(data_rows);
		jQuery('#result_set').submit();
	}
</script>