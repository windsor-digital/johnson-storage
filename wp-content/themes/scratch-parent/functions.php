<?php if (!defined('WP_DEBUG')) die('Direct access forbidden.');

require dirname(__FILE__) .'/framework/bootstrap.php';
?>
<?php
 
function show_disabled_buttons(){
    $r[] = 'code';
    $r[] = 'backcolorpicker';
    $r[] = 'forecolorpicker';
        return $r;
}
// Use the filter hook to enable the buttons
add_filter("mce_buttons_2", "show_disabled_buttons");


/******************* Excerpts fix ***************/
// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '<a class="moretag" href="'. get_permalink($post->ID) . '"> Read the full article...</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

  ?>
