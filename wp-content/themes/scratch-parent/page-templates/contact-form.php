<?php
/**
 * Template Name: Contact Form Page
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					get_template_part( 'content', 'page' );
					// results php stuff goes here
			?>
			
			<div class="quote-page">
				<h1 class="entry-title">Contact Me</h1>

				<div id="main-content quote results" class="main-content">

					<div id="quote-summary" class="quote">
						<h3>Quote ID #<?php echo $_SESSION['quote']->quote_requests['display_id']?></h3>

						
							<div id="quote-summary-from" class="quote-summary-figure">
								<?php if (preg_match('/^\d{5}$/', $_POST['addr_from'])) { ?>
									<h4>From Zip</h4>
									<p><?php echo $_POST['addr_from']?></p>
								<?php } else { ?>
									<h4>From Address</h4>
									<p><?php echo $_POST['addr_from']?></p>
								<?php } ?>
							</div>

							<div id="quote-summary-to" class="quote-summary-figure">
								<?php if (preg_match('/^\d{5}$/', $_POST['addr_to'])) { ?>
									<h4>To Zip</h4>
									<p><?php echo $_POST['addr_to']?></p>
								<?php } else { ?>
									<h4>To Address</h4>
									<p><?php echo $_POST['addr_to']?></p>
								<?php } ?>
							</div>
							<div class="clearfix"></div>
							<div id="quote-summary-rooms" class="quote-summary-figure">
								<h4>Rooms</h4>
								<p><?php echo $_POST['num_rooms']?> Rooms</p>
							</div>
							<div id="quote-summary-movepack">
								<h4>Moving<?php echo $_POST['packing'] ? ' + Packing' : ''?> Service</h4>
								<p>$<?php echo number_format($_POST["dollars". ($_POST['packing'] ? 'Pack' : 'Move')]) ?></p>
							</div>
					</div>
					<div class="clearfix"></div>

				<div id="book-move" class="quote">

					<p class="instr">Enter your contact information and we will call you to discuss your quote within the next 24 business hours.
						We will not share or sell your information.
						<span class="required"><span class="asterisk"><span>*</span></span> Required fields are marked with an asterisk.</span>
					</p>

					<form id="contact_form" action="/contact-results/" method="post">
						<p><label class="field-prompt" for="name">Name:<span class="asterisk"><span>*</span></span></label>
							<br />
							<input name="full_name" type="text" class="text name" value="<?php echo $_POST['full_name']?>" />
						</p>
						<p><label class="field-prompt" for="phone">Phone:<span class="asterisk"><span>*</span></span></label>
							<br />
							<input name="email" type="text" class="text email" value="<?php echo $_POST['phone']?>" />
						</p>
						<p><label class="field-prompt" for="email">Email:</label>
							<br />
							<input name="email" type="text" class="text email" value="<?php echo $_POST['email']?>" />
						</p>
						<p><label class="field-prompt" for="notes">Notes:</label>
							<br />
							<textarea id="contact_notes" cols="58" rows="8" class="notes" name="notes"></textarea>
						</p>
						<p class="submit"><input name="quote_contact" type="submit" class="button submit" value="Submit" /></p>
					</form>

				</div>

				<div id="error_div">
				</div>

				<div class="expander"></div>

				
				
			</div>
			<script type="text/javascript">

				function submitbookit() {
					var contact_data = {};
					jQuery('#contact_form').find('input, select').each(function(){
						if(jQuery(this).is(':checkbox') && jQuery(this).is(':checked')) {
							contact_data[jQuery(this).attr('name')] = jQuery(this).val()
						} else if(!jQuery(this).is(':checkbox')) {
							contact_data[jQuery(this).attr('name')] = jQuery(this).val();	
						}
					});
					contact_data['notes'] = jQuery('#contact_notes').val();
					console.log(contact_data);
					jQuery.ajax({
			            url: 'http://www.johnsonstoragedev.com/api/quote-app/app/jsoncontact.php',
			            data: contact_data, 
			            type: "POST",
			            success: function(data) {
			            	contact_result = JSON.parse(data);
			            	if(bookit_result.success) {
			            		jQuery('#contact_form').submit();
			            	} else {
			            		for(var i=0; i < contact_result.errors.length; i++) {
			            			jQuery('#error_div').append('<span>' + contact_result.errors[i] + '<span><br/>');	
			            		}	            		
			            	}
			            	
			            }
			        });
				}
			
			</script>


			<?php 		
				endwhile;
			?>
		</div><!-- #content -->
		<p class="disclaimer">
					This quote is a non-binding agreement. Instant Quote estimates are based
					on average weights and labor required to move a household with the number of
					rooms that you selected. If you choose to book the move, a service representative
					will contact you and provide a firm price. An on-site evaluation may be necessary
					(and sometimes required by law) to accurately determine the final price. Thank you
					for contacting Johnson Storage and Moving Company, quality movers since 1899.
				</p>
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();