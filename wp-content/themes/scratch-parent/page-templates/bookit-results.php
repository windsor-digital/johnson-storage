<?php
/**
 * Template Name: Bookit Results Page
 */

get_header(); ?>

<div id="main-content quote results" class="main-content">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					get_template_part( 'content', 'page' );
					// results php stuff goes here
			?>
			<div class="quote-page">
	<h1 class="entry-title">Book Your Move - Confirmation</h1>
	<div id="book-move" class="quote">

		<?php if ($_POST['isLocal'] == "true") { ?>
			<p class="conf">
				Thank you!
				We will call you to confirm your booking within the next 24 business hours.</p>
			
		<?php } else { ?>
			<p class="conf">
				Thank you for booking your move based on your non-binding estimate.
				United Van Lines, along with all quality movers, require a visual survey
				of the items you wish to move. We will contact you shortly to arrange a
				convenient time for that survey. At the end of the survey, we will
				provide you with your binding estimate and if you choose, we can lock in
				your dates and move forward. If you prefer, you can call us directly at
				800-289-6683 and you can schedule your survey now.				
			</p>
		<?php } ?>

	</div>

	<div class="expander"></div>

		<p class="disclaimer">
			This quote is a non-binding agreement. Instant Quote estimates are based
			on average weights and labor required to move a household with the number of
			rooms that you selected. If you choose to book the move, a service representative
			will contact you and provide a firm price. An on-site evaluation may be necessary
			(and sometimes required by law) to accurately determine the final price. Thank you
			for contacting Johnson Storage and Moving Company, quality movers since 1899.
		</p> <br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />

</div>
			<?php 		
				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();