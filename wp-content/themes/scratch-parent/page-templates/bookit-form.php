<?php
/**
 * Template Name: Bookit Form Page
 */

get_header(); ?>

<div id="main-content quote results" class="main-content">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					get_template_part( 'content', 'page' );
					// results php stuff goes here
					$list = array('','AL','AK','AZ','AR','CA','CO','CT','DE','DC','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY');
			?>


<div class="quote-page">
	<h1 class="entry-title">Book Your Move</h1>

	<div id="quote-summary" class="quote">
		<h3>Quote ID #<?php echo $_POST['display_id']?></h3>

			<div id="quote-summary-from" class="quote-summary-figure">
				<?php if (preg_match('/^\d{5}$/', $_POST['addr_from'])) { ?>
					<h4>From Zip</h4>
					<p><?php echo $_POST['addr_from']?></p>
				<?php } else { ?>
					<h4>From Address</h4>
					<p><?php echo $_POST['addr_from']?></p>
				<?php } ?>
			</div>

			<div id="quote-summary-to" class="quote-summary-figure">
				<?php if (preg_match('/^\d{5}$/', $_POST['addr_to'])) { ?>
					<h4>To Zip</h4>
					<p><?php echo $_POST['addr_to']?></p>
				<?php } else { ?>
					<h4>To Address</h4>
					<p><?php echo $_POST['addr_to']?></p>
				<?php } ?>
			</div>
			<div class="clearfix"></div>
			<div id="quote-summary-rooms" class="quote-summary-figure">
				<h4>Rooms</h4>
				<p><?php echo $_POST['num_rooms']?> Rooms</p>
			</div>
			<div id="quote-summary-movepack">
				<h4>Moving<?php echo $_POST['packing'] ? ' + Packing' : ''?> Service</h4>
				<p>$<?php echo number_format($_POST["dollars". ($_POST['packing'] ? 'Pack' : 'Move')]) ?></p>
			</div>
		<div class="clearfix"></div>
	</div>

	<div id="book-move" class="quote">

		<p class="instr">Enter your contact information and date of move.
			We will call you to confirm your booking within the next 24 business hours.
			We will not share or sell your information.
			<span class="required"><span class="asterisk"><span>*</span></span> Required fields are marked with an asterisk.</span>
		</p>

		<div id="error_div">
		</div>

		<form id="bookit_data" action="/bookit-results/" method="post">
			<input type="hidden" name="isLocal" value="<?php echo $_POST['isLocal']?>" />
			<input type="hidden" name="packing" value="<?php echo $_POST['packing'] ? 1 : 0?>" />
			<p><label class="field-prompt" for="name">Name:<span class="asterisk"><span>*</span></span></label>
				<br />
				<input name="full_name" type="text" class="text name" value="<?php echo htmlspecialchars($_POST['full_name'])?>" />
			</p>
			<p><label class="field-prompt" for="phone">Phone:<span class="asterisk"><span>*</span></span></label>
				<br />
				<input name="email" type="text" class="text email" value="<?php echo htmlspecialchars($_POST['phone'])?>" />
			</p>
			<p><label class="field-prompt" for="email">Email:</label>
				<br />
				<input name="email" type="text" class="text email" value="<?php echo htmlspecialchars($_POST['email'])?>" />
			</p>

			<p>
				<label class="field-prompt">From Address:</label>
			</p>
			<table class="address">
				<tr>
					<th><label class="field-prompt" for="from_addr1">Line 1:</label></th>
					<td><input name="from_addr1" type="to_text" class="text addr line addr1" value="" /></td>
				</tr>
				<tr>
					<th><label class="field-prompt" for="from_addr2">Line 2:</label></th>
					<td><input name="from_addr2" type="text" class="text addr line addr2" value="" /></td>
				</tr>
				<tr>
					<th><label class="field-prompt" for="from_city">City:<span class="asterisk"><span>*</span></span></label></th>
					<td>
						<input name="from_city" type="text" class="text addr city" value="" />

						<label class="field-prompt" for="from_state" class="inline">State:<span class="asterisk"><span>*</span></span></label>
						<select name="from_state" class="addr state">
							<?php foreach($list as $st) { ?>
								<option value="<?php echo $st?>"<?php echo $st == $_POST['orignState'] ? ' selected="selected"' : ''?>><?php echo $st?> </option>
							<?php } ?>
						</select>

						<label class="field-prompt" for="from_zip" class="inline">Zip:<span class="asterisk"><span>*</span></span></label>
						<input name="from_zip" type="text" class="text addr zip" value="<?php echo htmlspecialchars($_POST['addr_from'])?>" />
					</td>
				</tr>
			</table>
			
			<p>
				<label class="field-prompt">To Address:</label>
			</p>
			<table class="address">
				<tr>
					<th><label class="field-prompt" for="to_addr1">Line 1:</label></th>
					<td><input name="to_addr1" type="text" class="text addr line addr1" value="" /></td>
				</tr>
				<tr>
					<th><label class="field-prompt" for="to_addr2">Line 2:</label></th>
					<td><input name="to_addr2" type="text" class="text addr line addr2" value="" /></td>
				</tr>
				<tr>
					<th><label class="field-prompt" for="to_city">City:<span class="asterisk"><span>*</span></span></label></th>
					<td>
						<input name="to_city" type="text" class="text addr city" value="" />

						<label class="field-prompt" for="to_state" class="inline">State:<span class="asterisk"><span>*</span></span></label>
						<select name="to_state" class="addr state">
							<?php foreach($list as $st) { ?>
								<option value="<?php echo $st?>"<?php echo $st == $_POST['destState'] ? ' selected="selected"' : ''?>><?php echo $st?> </option>
							<?php } ?>
						</select>

						<label class="field-prompt" for="to_zip" class="inline">Zip:<span class="asterisk"><span>*</span></span></label>
						<input name="to_zip" type="text" class="text addr zip" value="<?php echo htmlspecialchars($_POST['addr_to'])?>" />
					</td>
				</tr>
			</table>

			<p><label class="field-prompt" for="mdate">Move Date:<span class="asterisk"><span>*</span></span> <span class="instr">(e.g. mm/dd/yyyy)</span></label>
				<br />
				<input name="mdate" id="mdate" type="text" class="text date" value="" />
			</p>
			<p><span class="field-prompt">Special Considerations:</span>
				<span class="option-list">
						<br />
					<label>
						<input type="checkbox" name="special1" value="Items must be carried over 100 ft. at origin or destination" /> Items must be carried over 100 ft. at origin or destination					</label>
									<br />
					<label>
						<input type="checkbox" name="special2" value="There are unusual or bulky items" /> There are unusual or bulky items					</label>
									<br />
					<label>
						<input type="checkbox" name="special3" value="The origin or destination is not accessible by tractor trailer" /> The origin or destination is not accessible by tractor trailer					</label>
									<br />
					<label>
						<input type="checkbox" name="special4" value="There are one or more flights of stairs at origin or destination" /> There are one or more flights of stairs at origin or destination					</label>
			
				</span>
			</p>
			<p><label class="field-prompt" for="notes">Additional Notes:</label>
				<textarea id="bookit_notes" cols="58" rows="3" class="notes" name="notes"></textarea>
			</p>
			<p><span class="field-prompt">I Want to Know More about Additional Services:</span>
				<span class="option-list">
					<br />
					<label>
						<input type="checkbox" name="addl1" value="Storage" /> Storage					</label>
									<br />
					<label>
						<input type="checkbox" name="addl2" value="Home theater assembly/disassembly" /> Home theater assembly/disassembly					</label>
									<br />
					<label>
						<input type="checkbox" name="addl3" value="Computer network assembly/disassembly" /> Computer network assembly/disassembly					</label>
								</span>
				</span>
			</p>
			<p class="submit">
				<input name="bookit" type="submit" onclick="submitbookit(); return false;" class="button submit" value="Book Move" />
			</p>
		</form>

	</div>

	<div class="expander"></div>	
</div>


	<script type="text/javascript">

		function submitbookit() {
			var bookit_data = {};
			jQuery('#bookit_data').find('input, select').each(function(){
				if(jQuery(this).is(':checkbox') && jQuery(this).is(':checked')) {
					bookit_data[jQuery(this).attr('name')] = jQuery(this).val()
				} else if(!jQuery(this).is(':checkbox')) {
					bookit_data[jQuery(this).attr('name')] = jQuery(this).val();	
				}
			});
			bookit_data['notes'] = jQuery('#bookit_notes').val();			
			console.log(bookit_data);
			jQuery.ajax({
	            url: 'http://www.johnsonstoragedev.com/api/quote-app/app/jsonbookit.php',
	            data: bookit_data), 
	            type: "POST",
	            success: function(data) {
	            	bookit_result = JSON.parse(data);
	            	if(bookit_result.success) {
	            		jQuery('#bookit_data').submit();
	            	} else {
	            		for(var i=0; i < bookit_result.errors.length; i++) {
	            			jQuery('#error_div').append('<span>' + bookit_result.errors[i] + '<span><br/>');	
	            		}	            		
	            	}
	            	
	            }
	        });
		}
	
	</script>			
			<?php 		
				endwhile;
			?>
		
		<p class="disclaimer">
		This quote is a non-binding agreement. Instant Quote estimates are based
		on average weights and labor required to move a household with the number of
		rooms that you selected. If you choose to book the move, a service representative
		will contact you and provide a firm price. An on-site evaluation may be necessary
		(and sometimes required by law) to accurately determine the final price. Thank you
		for contacting Johnson Storage and Moving Company, quality movers since 1899.
</p>	</div>
		<!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();