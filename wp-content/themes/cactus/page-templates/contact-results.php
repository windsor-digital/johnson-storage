<?php
/**
 * Template Name: Contact Results Page
 */

get_header(); ?>

<div id="main-content quote results" class="main-content">


	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
				// Start the Loop.
				while ( have_posts() ) : the_post();

					get_template_part( 'content', 'page' );
					// results php stuff goes here
			?>
			<h1 class="entry-title">Contact Results</h1>

			<div id="book-move" class="quote">

					<p class="conf">
						Thank you!
						We will call you within the next 24 business hours.</p>

			</div>

			<div class="expander"></div>
			<p class="disclaimer">
				This quote is a non-binding agreement. Instant Quote estimates are based
				on average weights and labor required to move a household with the number of
				rooms that you selected. If you choose to book the move, a service representative
				will contact you and provide a firm price. An on-site evaluation may be necessary
				(and sometimes required by law) to accurately determine the final price. Thank you
				for contacting Johnson Storage and Moving Company, quality movers since 1899.
			</p>
			<br /><br /><br /><br /><br /><br />
			
			<?php 		
				endwhile;
			?>
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();