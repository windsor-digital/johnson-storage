<?php
/**
 * Template Name: Quote Results Page 1
 */

get_header(); ?>

<div id="main-content quote results" class="main-content">


    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();

                    get_template_part( 'content', 'page' );
                    // results php stuff goes here

                $referralSources = array(
                    'Realtor Office/Kiosk',
                    'Realtor Referral',
                    'Internet Search',
                    'Friend/Family',
                    'Radio Ad',
                    'Other',
                );
            ?>                      
            <!-- Begin Result Sets -->
            <script type="text/javascript">
            /* <![CDATA[ */
            var google_conversion_id = 989535291;
            var google_conversion_language = "en";
            var google_conversion_format = "3";
            var google_conversion_color = "ffffff";
            var google_conversion_label = "2qQ1CPWM1QMQu7js1wM";
            var google_conversion_value = 0;
            if (1) {
               google_conversion_value = 1;
            }
            /* ]]> */
            </script>
            <script type="text/javascript"
            src="http://www.googleadservices.com/pagead/conversion.js">
            </script>
            <noscript>
            <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt=""
            src="http://www.googleadservices.com/pagead/conversion/989535291/?value=1&amp;label=2qQ1CPWM1QMQu7js1wM&amp;guid=ON&amp;script=0"/>
            </div>
            </noscript> 

            <div class="quote_top">
                <div class="inner">
                    <div class="column-2-3">
                        <h1 class="entry-title">Your Johnson Storage &amp; Moving Price Quote</h1>
                        <h3>Your Reference Number is <span><?php echo $_POST['id']?></span></h3>
                        <div class="action-box action-box-moving">
                            <div class="inner">
                                <div class="action_title">
                                    Moving Service
                                </div>
                                <div class="quote-figure">
                                    $<?php echo number_format($_POST['dollarsMove'])?>
                                </div>
                                <br clear="all">

                                <p class="details">
                                    Our highly trained professional movers will carefully move your furniture and boxes
                                    from your current home into your new home.
                                </p>

                                <div class="action-buttons">
                                    <form id="MovingBookit" action="/bookit-form/" method="post" class="service-button">
                                        <input type="hidden" name="packing" value="0" />
                                        <input type="hidden" name="dollarsMove" value="<?php echo $_POST['dollarsMove']?>" />
                                        <input type="hidden" name="isLocal" value="<?php echo $_POST['isLocal']?>" />                                       
                                        <input type="submit" name="bookit" onclick="submitBookitContact('#MovingBookit'); return false;" class="button submit bookit" value="Book It Now" style="margin-bottom:5px" />
                                    </form>
                                    <form id="MovingContact" action="/contact-form/" method="post" class="service-button">
                                        <input type="hidden" name="packing" value="0" />
                                        <input type="hidden" name="dollarsMove" value="<?php echo $_POST['dollarsMove']?>" />
                                        <input type="submit" name="contact" onclick="submitBookitContact('#MovingContact'); return false;" class="button contact plain" value="Contact Me" />
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="action-box action-box-packing">
                            <div class="inner">
                                <div class="action_title">
                                    Moving + Packing Service
                                </div>

                                <div class="quote-figure">
                                    $<?php echo number_format($_POST['dollarsPack'])?>
                                </div>
                                <br clear="all">

                                <p class="details">
                                    Our extensively trained packers will expertly wrap and place all your belongings into
                                    boxes and our movers will carefully move your furniture and boxes from your current
                                    home into your new home.
                                </p>

                                <div class="action-buttons">
                                    <form id="packingBookit" action="/bookit-form/" method="post" class="service-button">
                                        <input type="hidden" name="packing" value="1" />
                                        <input type="hidden" name="dollarsPack" value="<?php echo$_POST['dollarsPack']?>" />
                                        <input type="hidden" name="isLocal" value="<?php echo $_POST['isLocal']?>" />
                                        <input type="submit" name="bookit" onclick="submitBookitContact('#packingBookit'); return false;" class="button submit bookit" value="Book It Now" style="margin-bottom:5px" />
                                    </form>
                                    <form id="packingContact" action="/contact-form/" method="post" class="service-button">
                                        <input type="hidden" name="packing" value="1" />
                                        <input type="hidden" name="dollarsPack" value="<?php echo $_POST['dollarsPack']?>" />
                                        <input type="submit" name="contact" onclick="submitBookitContact('#packingContact'); return false;" class="button contact plain" value="Contact Me"   />
                                    </form>
                                </div>
                            </div>
                        </div>
                        <br clear="all" />
                        <p class="disclaimer">
                        This quote is a non-binding agreement. Instant Quote estimates are based
                        on average weights and labor required to move a household with the number of
                        rooms that you selected. If you choose to book the move, a service representative
                        will contact you and provide a firm price. An on-site evaluation may be necessary
                        (and sometimes required by law) to accurately determine the final price. Thank you
                        for contacting Johnson Storage and Moving Company, quality movers since 1899.
                        </p>
                        <div class="print">
                            <a onclick="window.print()" title="Click for Printer-Friendly Version">print</a>
                        </div>
                        <div class="quote_middle">
                            <div class="inner">
                                <div class="quote_top_left">
                                    <h1>Your Move Information</h1>
                                    <h3>Thank You! Your Reference Number is <span><?php echo $_POST['id']?></span></h3>
                                    <p class="big">A Johnson representative will be in contact with you to answer any questions that you have. Please retain your reference number for future assistance.</p>
                                </div>
                                <form id="resultForm" method="post">
                                    <input name="display_id" type="hidden" class="text date" value="<?php echo $_POST['display_id']?>" />
                                    <input name="mdate" type="hidden" class="text date" value="<?php echo $_POST['mdate']?>" />
                                    <input name="orignState" type="hidden" class="text full_name" value="<?php echo htmlspecialchars($_POST['orignState'])?>" />
                                    <input name="destState" type="hidden" class="text full_name" value="<?php echo htmlspecialchars($_POST['destState'])?>" />
                                    <input name="full_name" type="hidden" class="text full_name" value="<?php echo htmlspecialchars($_POST['full_name'])?>" />
                                    <input name="phone" type="hidden" class="text phone" value="<?php echo htmlspecialchars($_POST['phone'])?>" />
                                    <input name="email" type="hidden" class="text email" value="<?php echo htmlspecialchars($_POST['email'])?>" />
                                    <select name="source" style="display:none;">
                                        <?php foreach($referralSources as $k => $v) { ?>
                                        <option value="<?php echo $v ?>" <?php echo ($_POST['source'] == $v) ? "selected='selected'" : "" ?>><?php echo $v ?></option>
                                        <?php } ?>
                                    </select>
                                    <div id="quote-summary-figures">
                                    <div id="quote-summary-from" class="quote-summary-figure">
                                        <div class="summary_inner">
                                                    <h4>From Zip Code</h4>
                                            <input type="text" name="addr_from" value="<?php echo htmlspecialchars($_POST['orignZip'])?>">
                                                </div>
                                    </div>
        
                                    <div id="quote-summary-to" class="quote-summary-figure">
                                        <div class="summary_inner">
                                                    <h4>To Zip Code</h4>
                                            <input type="text" name="addr_to" value="<?php echo htmlspecialchars($_POST['destZip'])?>">
                                                </div>
                                    </div>
        
                                    <div id="quote-summary-rooms" class="quote-summary-figure">
                                        <div class="summary_inner">
                                            <h4>Number of Rooms</h4>
                                            <select name="num_rooms" class="rooms">
                                                <option value="">(Select One)</option>
                                                <?php $i = 1; while ($i++ < 20) { ?>
                                                <option value="<?php echo $i?>" <?php echo $i == $_POST['rooms'] ? ' selected="selected"' : ''?>><?php echo $i?> <?php echo ($i > 1) ? 'Rooms' : 'Room'?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <p class="please_include">Please Include all rooms including kitchen, basement, bedrooms, etc.</p>
                                    <input name="getquote" type="submit" onclick="get_quote_json(); return false;" class="button submit plain" value="Recalculate" />
                                </form>
                                <div id="error_div"></div>
                                <div style="display:none;">
                                    <form id="result_set" action="/quote-results/" method="POST">
                                    </form>
                                </div>
                                <div class="map" style="margin-bottom:20px">
                                    <?php echo "<img src='" . $_POST['map'] . "' alt='map' border='0'>"?>
                                </div>
                                
                            </div>
                        </div>



                    </div>
                    <!--end of column -->
                    </div>
                    <div class="column-1-3">
                        <h1>Know What's Included In Your<br/> Moving Price Quote</h1>
                        <p>Not all moving quotes are created equal. At Johnson Storage & Moving we provide our customers with price quotes that include many of the following items our competitors don't.</p>
                        <h3><span>Included in Every Price Quote</span></h3>
                        <ul id="included">
                            <li>Background Checked Movers</li>
                            <li>Insured</li>
                            <li>Home Protection</li>
                            <li>Upholstery Protection</li>
                            <li>Mattress Protection</li>
                            <li>Flat Screen TV Protection</li>
                            <li>Clothing Protection</li>
                            <li>Painting Protection</li>
                            <li class="last">Better Business Bureau Member</li>
                        </ul>
                    </div>

                </div>
            </div>
            

            <script type="text/javascript">


                function get_quote_json() {
                    //var quote_query = $('#quote_form').serialize();
                    var quote_data = getQuoteData();
                    jQuery.ajax({
                        url: 'http://www.johnsonstorage.com/api/quote-app/app/jsonresult.php',
                        data: quote_data, 
                        type: "POST",
                        success: function(data) {
                            quote_result = JSON.parse(data);
                            if (quote_result.success) {
                                submitResults(quote_result);
                            } else {
                                for(var i=0; i < quote_result.errors.length; i++) {
                                    jQuery('#error_div').append('<span>' + quote_result.errors[i] + '<span><br/>'); 
                                }
                            }
                        }
                    });
                }

                function submitResults(quote_results) {
                    data_rows = "";
                    for (var key in quote_results) {
                        if(key == 'containsValue') {
                            data_row = "";
                        } else if(key == 'map') { 
                            data_row = "<textarea name='" + key + "' >" + quote_results[key] + "</textarea>";
                        } else {
                            data_row = "<input name='" + key + "' type='text' value='" + quote_results[key] + "'>";
                        }
                        data_rows = data_rows + data_row;           
                    }
                    jQuery('#result_set').empty();
                    jQuery('#result_set').html(data_rows);
                    jQuery('#result_set').submit();
                }

                function getQuoteData() {
                    var quote_data = {};
                    jQuery('#resultForm').find('input, select').each(function(){
                        quote_data[jQuery(this).attr('name')] = jQuery(this).val();
                    });
                    return quote_data
                }

                function submitBookitContact(form_name) {
                    var quote_data = getQuoteData();
                    data_rows = "";
                    for (var key in quote_data) {
                        if(key == 'containsValue') {
                            data_row = "";
                        } else {
                            data_row = "<input name='" + key + "' type='hidden' value='" + quote_data[key] + "'>";
                        }
                        data_rows = data_rows + data_row;           
                    }
                    jQuery(form_name).append(data_rows);
                    jQuery(form_name).submit();                 
                }
            </script>

            <!-- End Begin Result Sets -->      
            <?php       
                endwhile;
            ?>
        </div><!-- #content -->
    </div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
